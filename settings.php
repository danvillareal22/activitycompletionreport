<?php

defined('MOODLE_INTERNAL') || die;

// $ADMIN->add('reports', new admin_category('report_activitycompletionreport_parent', get_string('pluginname', 'report_activitycompletionreport')));
$ADMIN->add('reports', new admin_externalpage('report_activitycompletionreport/acr', 'Detailed Course Completion Report', "$CFG->wwwroot/report/activitycompletionreport/index.php"));

$settings = null;