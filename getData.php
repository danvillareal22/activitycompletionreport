<?php
require('../../config.php');
require_once('../../user/profile/lib.php');
require_once($CFG->libdir . '/completionlib.php');
global $DB;

// Get course
$id = optional_param('course',null,PARAM_INT);
$transaction = $DB->start_delegated_transaction();
$course = $DB->get_record('course',array('id'=>$id));
$context = context_course::instance($course->id);

// Whether to show extra user identity information
$extrafields = get_extra_user_fields($context);

$completion = new completion_info($course);
$activities = $completion->get_activities();

$progress = array();

$progress = $completion->get_progress_all(
    null,
    '',
    '',
    '',
    '',
    '',
    $context
);
$transaction->allow_commit();
$data = new stdClass();
$columns = array();
array_push($columns,array("Employee No"), array("Full Name"));
foreach ($extrafields as $field) {
	array_push($columns, array(get_user_field_name($field)));
} 
array_push($columns,array("Area"),array("Zone"),array("Station"));

foreach($activities as $activity) {
    $datepassed = $activity->completionexpected && $activity->completionexpected <= time();
    $datepassedclass = $datepassed ? 'completion-expired' : '';

    if ($activity->completionexpected) {
        $datetext=userdate($activity->completionexpected,get_string('strftimedate','langconfig'));
    } else {
        $datetext='';
    }

    // Some names (labels) come URL-encoded and can be very long, so shorten them
    $displayname = format_string($activity->name, true, array('context' => $activity->context));
    

        $shortenedname = shorten_text($displayname);
        $mod = '<a href="'.$CFG->wwwroot.'/mod/'.$activity->modname.
        '/view.php?id='.$activity->id.'" title="' . s($displayname) . '">'.
        '<img src="'.$OUTPUT->pix_url('icon', $activity->modname).'" alt="'.
        s(get_string('modulename', $activity->modname)).
            '" /> <span class="completion-activityname">'.
        $shortenedname.'</span></a>';
        array_push($columns, array($mod), array("Completion Date"));

}
array_push($columns, array("Course Completion"));
$data->columns = $columns;

$rows = array();

foreach($progress as $user) {
    $userdata = array();
    // User name
	profile_load_data($user);
    
    $name = '<a href="'.$CFG->wwwroot.'/user/view.php?id='.
    $user->id.'&amp;course='.$course->id.'">'.fullname($user).'</a>';
    array_push($userdata, $user->idnumber,$name);
    foreach ($extrafields as $field) {
        array_push($userdata, s($user->{$field}));
    }
    array_push($userdata, $user->profile_field_section, $user->profile_field_subsection, $user->profile_field_location);
   

    // Progress for each activity
    foreach($activities as $activity) {

        // Get progress information and state
        if (array_key_exists($activity->id,$user->progress)) {
            $thisprogress=$user->progress[$activity->id];
            $state=$thisprogress->completionstate;
            if($thisprogress->timemodified == 0){
                $date='';
            }else{
                $date=userdate($thisprogress->timemodified);
            }
            
        } else {
            $state=COMPLETION_INCOMPLETE;
            $date='';
        }

        // Work out how it corresponds to an icon
        switch($state) {
            case COMPLETION_INCOMPLETE : $completiontype='n'; break;
            case COMPLETION_COMPLETE : $completiontype='y'; break;
            case COMPLETION_COMPLETE_PASS : $completiontype='pass'; break;
            case COMPLETION_COMPLETE_FAIL : $completiontype='fail'; break;
        }

        $completionicon='completion-'.
            ($activity->completion==COMPLETION_TRACKING_AUTOMATIC ? 'auto' : 'manual').
            '-'.$completiontype;

        $describe = get_string('completion-' . $completiontype, 'completion');
        $a=new StdClass;
        $a->state=$describe;
        $a->date=$date;
        $a->user=fullname($user);
        $a->activity = $formattedactivities[$activity->id]->displayname;
        $fulldescribe=get_string('progress-title','completion',$a);

        $moddata = '<img src="'.$OUTPUT->pix_url('i/'.$completionicon).
        '" alt="'.s($describe).'" title="'.s($fulldescribe).'" />';
        array_push($userdata, $moddata, $a->date);
        }
        $cc = $DB->get_record_sql("SELECT * FROM mdl_course_completions WHERE userid = ? AND course = ?", array($user->id,$course->id));

        if(isset($cc->timecompleted)){
            array_push($userdata, "Yes");
        }else{
            array_push($userdata, "No");
        }

        array_push($rows ,$userdata);
    }

$data->data = $rows;
$data->scrollX = true;
$data->responsive = true;
$data->sPaginationType ="full_numbers";
$data->lengthMenu = [[10, 25, 50, -1], [10, 25, 50, "All"]];

echo json_encode($data);