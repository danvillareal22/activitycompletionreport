<?php
error_reporting(0);
require('../../config.php');
require_once('../../user/profile/lib.php');
require_once($CFG->libdir . '/completionlib.php');

define('COMPLETION_REPORT_PAGE', 25);

// Get course
$id = optional_param('course',null,PARAM_INT);
if($id){
	
}
$course = $DB->get_record('course',array('id'=>$id));
if (!$course) {
	$PAGE->set_context(context_system::instance());
	$PAGE->set_heading('Detailed Course Completion Report');
	$url = new moodle_url('/report/activitycompletionreport/csv.php');
	$PAGE->set_url($url);
    echo $OUTPUT->header();
	echo $OUTPUT->container('<h1>Detailed Course Completion Report</h1>');
    $courses = $DB->get_records_sql("SELECT * FROM mdl_course");
	print '<form name = "courselect" action="#" method="POST" style="float:right;">';
	print 'Course: <select name="course">';
	foreach($courses as $c){
		print '<option value ='.$c->id.'>'.$c->fullname.'</option>';
	}
	print '</select>';
	print '<input type="submit" value="Go">';
	print '</form>';
	echo $OUTPUT->footer();
	exit;
	
}

$context = context_course::instance($course->id);
// Sort (default lastname, optionally firstname)
$sort = optional_param('sort','',PARAM_ALPHA);
$firstnamesort = $sort == 'firstname';

// CSV format
$format = optional_param('format','',PARAM_ALPHA);
$excel = $format == 'excelcsv';
$csv = $format == 'csv' || $excel;

// Paging
$start   = optional_param('start', 0, PARAM_INT);
$sifirst = optional_param('sifirst', 'all', PARAM_NOTAGS);
$silast  = optional_param('silast', 'all', PARAM_NOTAGS);
$start   = optional_param('start', 0, PARAM_INT);

// Whether to show extra user identity information
$extrafields = get_extra_user_fields($context);
//var_dump($extrafields);
$leftcols = 1 + count($extrafields);

function csv_quote($value) {
    global $excel;
    if ($excel) {
        return core_text::convert('"'.str_replace('"',"'",$value).'"','UTF-8','UTF-16LE');
    } else {
        return '"'.str_replace('"',"'",$value).'"';
    }
}

$url = new moodle_url('/report/activitycompletionreport/csv.php', array('course'=>$id));
if ($sort !== '') {
    $url->param('sort', $sort);
}
if ($format !== '') {
    $url->param('format', $format);
}
if ($start !== 0) {
    $url->param('start', $start);
}
$PAGE->set_url($url);
$PAGE->set_pagelayout('report');
require_login();

// Check basic permission
require_capability('report/activitycompletionreport:view',$context);

// Get group mode
$group = groups_get_course_group($course,true); // Supposed to verify group
if ($group===0 && $course->groupmode==SEPARATEGROUPS) {
    require_capability('moodle/site:accessallgroups',$context);
}

// Get data on activities and progress of all users, and give error if we've
// nothing to display (no users or no activities)
$reportsurl = $CFG->wwwroot.'/course/report.php?id='.$course->id;
$completion = new completion_info($course);
$activities = $completion->get_activities();

// Generate where clause
$where = array();
$where_params = array();

if ($sifirst !== 'all') {
    $where[] = $DB->sql_like('u.firstname', ':sifirst', false);
    $where_params['sifirst'] = $sifirst.'%';
}

if ($silast !== 'all') {
    $where[] = $DB->sql_like('u.lastname', ':silast', false);
    $where_params['silast'] = $silast.'%';
}

// Get user match count
$total = $completion->get_num_tracked_users(implode(' AND ', $where), $where_params, $group);
// Total user count
$grandtotal = $completion->get_num_tracked_users('', array(), $group);

// Get user data
$progress = array();

if ($total) {
    $progress = $completion->get_progress_all(
        null,
        '',
        '',
        '',
        '',
        '',
        $context
    );
}
if ($csv && $grandtotal && count($activities)>0) { // Only show CSV if there are some users/actvs

    $shortname = format_string($course->shortname, true, array('context' => $context));
    header('Content-Disposition: attachment; filename=progress.'.
        preg_replace('/[^a-z0-9-]/','_',core_text::strtolower(strip_tags($shortname))).'.csv');
    // Unicode byte-order mark for Excel
    if ($excel) {
        header('Content-Type: text/csv; charset=UTF-16LE');
        print chr(0xFF).chr(0xFE);
        $sep="\t".chr(0);
        $line="\n".chr(0);
    } else {
        header('Content-Type: text/csv; charset=UTF-8');
        $sep=",";
        $line="\n";
    }
} else {

    // Navigation and header
    $strreports = get_string("reports");
    $strcompletion = get_string('activitycompletion', 'completion');

    $PAGE->set_title($strcompletion);
    $PAGE->set_heading($course->fullname);
	$PAGE->requires->jquery();
$PAGE->requires->js('/report/activitycompletionreport/js/jquery-ui.js',true);
	$PAGE->requires->js('/report/activitycompletionreport/js/jquery.dataTables.yadcf.js',true);
    $PAGE->requires->js('/report/activitycompletionreport/js/jquery.datatables.min.js',true);
	$PAGE->requires->js('/report/activitycompletionreport/js/ac.js',true);
	$PAGE->requires->css('/report/activitycompletionreport/css/jquery-ui.css');
	$PAGE->requires->css('/report/activitycompletionreport/css/jquery.dataTables.min.css');
	$PAGE->requires->css('/report/activitycompletionreport/css/jquery.dataTables.yadcf.css');
    echo $OUTPUT->header();
	echo $OUTPUT->container('<h1>Detailed Course Completion Report</h1>');
	
    
    // $PAGE->requires->js_function_call('textrotate_init', null, true);

    // Handle groups (if enabled)
    groups_print_course_menu($course,$CFG->wwwroot.'/report/activitycompletionreport/?course='.$course->id);
}

if (count($activities)==0) {
    echo $OUTPUT->container(get_string('err_noactivities', 'completion'), 'errorbox errorboxcontent');
    echo $OUTPUT->footer();
    exit;
}

// If no users in this course what-so-ever
if (!$grandtotal) {
    echo $OUTPUT->container(get_string('err_nousers', 'completion'), 'errorbox errorboxcontent');
    echo $OUTPUT->footer();
    exit;
}

// Build link for paging
$link = $CFG->wwwroot.'/report/activitycompletionreport/?course='.$course->id;
if (strlen($sort)) {
    $link .= '&amp;sort='.$sort;
}
$link .= '&amp;start=';

// Build the the page by Initial bar
$initials = array('first', 'last');
$alphabet = explode(',', get_string('alphabet', 'langconfig'));

$pagingbar = '';
foreach ($initials as $initial) {
    $var = 'si'.$initial;

    $othervar = $initial == 'first' ? 'silast' : 'sifirst';
    $othervar = $$othervar != 'all' ? "&amp;{$othervar}={$$othervar}" : '';

    // $pagingbar .= ' <div class="initialbar '.$initial.'initial">';
    // $pagingbar .= get_string($initial.'name').':&nbsp;';

    // if ($$var == 'all') {
        // $pagingbar .= '<strong>'.get_string('all').'</strong> ';
    // }
    // else {
        // $pagingbar .= "<a href=\"{$link}{$othervar}\">".get_string('all').'</a> ';
    // }

    // foreach ($alphabet as $letter) {
        // if ($$var === $letter) {
            // $pagingbar .= '<strong>'.$letter.'</strong> ';
        // }
        // else {
            // $pagingbar .= "<a href=\"$link&amp;$var={$letter}{$othervar}\">$letter</a> ";
        // }
    // }

    // $pagingbar .= '</div>';
}

// Do we need a paging bar?
if ($total > COMPLETION_REPORT_PAGE) {

    // Paging bar
    $pagingbar .= '<div class="paging">';
    $pagingbar .= get_string('page').': ';

    $sistrings = array();
    if ($sifirst != 'all') {
        $sistrings[] =  "sifirst={$sifirst}";
    }
    if ($silast != 'all') {
        $sistrings[] =  "silast={$silast}";
    }
    $sistring = !empty($sistrings) ? '&amp;'.implode('&amp;', $sistrings) : '';

    // Display previous link
    if ($start > 0) {
        $pstart = max($start - COMPLETION_REPORT_PAGE, 0);
        $pagingbar .= "(<a class=\"previous\" href=\"{$link}{$pstart}{$sistring}\">".get_string('previous').'</a>)&nbsp;';
    }

    // Create page links
    $curstart = 0;
    $curpage = 0;
    while ($curstart < $total) {
        $curpage++;

        if ($curstart == $start) {
            $pagingbar .= '&nbsp;'.$curpage.'&nbsp;';
        } else {
            $pagingbar .= "&nbsp;<a href=\"{$link}{$curstart}{$sistring}\">$curpage</a>&nbsp;";
        }

        $curstart += COMPLETION_REPORT_PAGE;
    }

    // Display next link
    $nstart = $start + COMPLETION_REPORT_PAGE;
    if ($nstart < $total) {
        $pagingbar .= "&nbsp;(<a class=\"next\" href=\"{$link}{$nstart}{$sistring}\">".get_string('next').'</a>)';
    }

    $pagingbar .= '</div>';
}

// Okay, let's draw the table of progress info,

// Start of table
if (!$csv) {
    print '<br class="clearer"/>'; // ugh

    //print $pagingbar;

    if (!$total) {
		echo $OUTPUT->container('<h1>Detailed Course Completion Report</h1>');
        echo $OUTPUT->heading(get_string('nothingtodisplay'));
        echo $OUTPUT->footer();
        exit;
    }
	
    print '<div id="completion-progress-wrapper">';
	$courses = $DB->get_records_sql("SELECT * FROM mdl_course");
	print '<form name = "courselect" style="float:right;">';
	print 'Course: <select name="course">';
	foreach($courses as $c){
		if($course->id == $c->id){
			print '<option value ='.$c->id.' selected>'.$c->fullname.'</option>';
		}else{
			print '<option value ='.$c->id.'>'.$c->fullname.'</option>';
		}
		
	}
	print '</select>';
	print '<input type="submit" value="Go">';
	print '</form>';
	print '<table cellspacing="5" cellpadding="5" border="0">
        <tbody>
		<tr>
            <td>Employee No:</td>
			<td><span id="empno"></span></td>
        </tr>
        <tr>
            <td>Full Name:</td>
			<td><span id="fname"></span></td>
        </tr>
		<tr>
            <td>Email Address:</td>
			<td><span id="email"></span></td>
        </tr>
		<tr>
            <td>Area:</td>
			<td><span id="area"></span></td>
        </tr>
		<tr>
            <td>Zone:</td>
			<td><span id="zone"></span></td>
        </tr>
		<tr>
            <td>Station:</td>
			<td><span id="station"></span></td>
        </tr>
    </tbody></table>';
    print '<table id="completion-progress" class="generaltable flexible boxaligncenter" style="text-align:left"><thead><tr style="vertical-align:top">';

    // User heading / sort option
	print '<th>Employee Number</th>';
    //print '<th scope="col" class="completion-sortchoice">';

    //$sistring = "&amp;silast={$silast}&amp;sifirst={$sifirst}";
    // if ($firstnamesort) {
        // print
            // get_string('firstname')." / <a href=\"./?course={$course->id}{$sistring}\">".
            // get_string('lastname').'</a>';
    // } else {
        // print "<a href=\"./?course={$course->id}&amp;sort=firstname{$sistring}\">".
            // get_string('firstname').'</a> / '.
            // get_string('lastname');
    // }
	
    print '<th>Full Name</th>';

    // Print user identity columns
    foreach ($extrafields as $field) {
        // echo '<th scope="col" class="completion-identifyfield">' .
		echo '<th>'. get_user_field_name($field) . '</th>';
    }
		print '<th> Area (Section) </th>';
		print '<th>Zone (Subsection)</th>';
		print '<th> Station (Location) </th>';
} else {
	echo csv_quote('Employee Number');
	echo $sep . csv_quote('Full Name');
    foreach ($extrafields as $field) {
        echo $sep . csv_quote(get_user_field_name($field));
    }
	echo $sep . csv_quote('Area (Section)');
	echo $sep . csv_quote('Zone (Subsection)');
	echo $sep . csv_quote('Station (Location)');
}

// Activities
$formattedactivities = array();
foreach($activities as $activity) {
    $datepassed = $activity->completionexpected && $activity->completionexpected <= time();
    $datepassedclass = $datepassed ? 'completion-expired' : '';

    if ($activity->completionexpected) {
        $datetext=userdate($activity->completionexpected,get_string('strftimedate','langconfig'));
    } else {
        $datetext='';
    }

    // Some names (labels) come URL-encoded and can be very long, so shorten them
    $displayname = format_string($activity->name, true, array('context' => $activity->context));
	
    if ($csv) {
        print $sep.csv_quote($displayname).$sep.csv_quote('Completion Date');
    } else {
        $shortenedname = shorten_text($displayname);
        print '<th scope="col" class="'.$datepassedclass.'">'.
            '<a href="'.$CFG->wwwroot.'/mod/'.$activity->modname.
            '/view.php?id='.$activity->id.'" title="' . s($displayname) . '">'.
            '<img src="'.$OUTPUT->pix_url('icon', $activity->modname).'" alt="'.
            s(get_string('modulename', $activity->modname)).
                '" /> <span class="completion-activityname">'.
            $shortenedname.'</span></a>';
        if ($activity->completionexpected) {
            print '<div class="completion-expected"><span>'.$datetext.'</span></div>';
        }
        print '</th>';
		print '<th>Completion Date</th>';
    }
    $formattedactivities[$activity->id] = (object)array(
        'datepassedclass' => $datepassedclass,
        'displayname' => $displayname,
    );
}
	
if ($csv) {
	print  $sep.csv_quote('Course Completion');
    print $line;
} else {
	print '<th>Course Completion</th>';
    print '</tr></thead><tbody>';
}

// Row for each user
foreach($progress as $user) {
    // User name
	profile_load_data($user);
    if ($csv) {
		print csv_quote($user->id);
        print $sep .csv_quote(fullname($user));
        foreach ($extrafields as $field) {
            echo $sep . csv_quote($user->{$field});
        }
		print $sep .csv_quote($user->profile_field_section);
		print $sep .csv_quote($user->profile_field_subsection);
		print $sep .csv_quote($user->profile_field_location);
    } else {
        print '<tr><td>'.$user->id.'</td><th scope="row"><a href="'.$CFG->wwwroot.'/user/view.php?id='.
            $user->id.'&amp;course='.$course->id.'">'.fullname($user).'</a></th>';
        foreach ($extrafields as $field) {
            echo '<td>' . s($user->{$field}) . '</td>';
        }
		print '<td>'.$user->profile_field_section.'</td>';
		print '<td>'.$user->profile_field_subsection.'</td>';
		print '<td>'.$user->profile_field_location.'</td>';

    }

    // Progress for each activity
    foreach($activities as $activity) {

        // Get progress information and state
        if (array_key_exists($activity->id,$user->progress)) {
            $thisprogress=$user->progress[$activity->id];
            $state=$thisprogress->completionstate;
            if($thisprogress->timemodified == 0){
                $date='';
            }else{
                $date=userdate($thisprogress->timemodified);
            }
        } else {
            $state=COMPLETION_INCOMPLETE;
            $date='';
        }

        // Work out how it corresponds to an icon
        switch($state) {
            case COMPLETION_INCOMPLETE : $completiontype='n'; break;
            case COMPLETION_COMPLETE : $completiontype='y'; break;
            case COMPLETION_COMPLETE_PASS : $completiontype='pass'; break;
            case COMPLETION_COMPLETE_FAIL : $completiontype='fail'; break;
        }

        $completionicon='completion-'.
            ($activity->completion==COMPLETION_TRACKING_AUTOMATIC ? 'auto' : 'manual').
            '-'.$completiontype;

        $describe = get_string('completion-' . $completiontype, 'completion');
        $a=new StdClass;
        $a->state=$describe;
        $a->date=$date;
        $a->user=fullname($user);
        $a->activity = $formattedactivities[$activity->id]->displayname;
        $fulldescribe=get_string('progress-title','completion',$a);

        if ($csv) {
            print $sep.csv_quote($describe).$sep.csv_quote($date);
        } else {
            print '<td class="completion-progresscell '.$formattedactivities[$activity->id]->datepassedclass.'">'.
                '<img src="'.$OUTPUT->pix_url('i/'.$completionicon).
                '" alt="'.s($describe).'" title="'.s($fulldescribe).'" /></td><td>'.$a->date.'</td>';
        }
    }
	
	$cc = $DB->get_record_sql("SELECT * FROM mdl_course_completions WHERE userid = ? AND course = ?", array($user->id,$course->id));
    if ($csv) {
		if(isset($cc->timecompleted)){
			print $sep.csv_quote('Yes');
		}else{
			print $sep.csv_quote('No');
		}
        print $line;
		
    } else {
		if(isset($cc->timecompleted)){
			print '<td> Yes </td>';
		}else{
			print '<td> No </td>';
		}
        print '</tr>';
    }
}

if ($csv) {
    exit;
}
print '</tbody></table>';
print '</div>';
//print $pagingbar;

print '<ul class="progress-actions"><li><a href="csv.php?course='.$course->id.
    '&amp;format=csv">'.get_string('csvdownload','completion').'</a></li>
    <li><a href="csv.php?course='.$course->id.'&amp;format=excelcsv">'.
    get_string('excelcsvdownload','completion').'</a></li></ul>';

echo $OUTPUT->footer();

