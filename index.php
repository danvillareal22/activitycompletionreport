<?php
error_reporting(0);
require('../../config.php');
require_once('../../user/profile/lib.php');
require_once($CFG->libdir . '/completionlib.php');

$id = optional_param('course',null,PARAM_INT);

$course = $DB->get_record('course',array('id'=>$id));
$courses = $DB->get_records_sql("SELECT * FROM mdl_course");
if (!$course) {
	$PAGE->set_context(context_system::instance());
	$PAGE->set_heading('Detailed Course Completion Report');
	$url = new moodle_url('/report/activitycompletionreport/csv.php');
	$PAGE->set_url($url);
    echo $OUTPUT->header();
	echo $OUTPUT->container('<h1>Detailed Course Completion Report</h1>');
    $courses = $DB->get_records_sql("SELECT * FROM mdl_course");
	print '<form name = "courselect" action="#" method="POST" style="float:right;">';
	print 'Course: <select name="course">';
	foreach($courses as $c){
		print '<option value ='.$c->id.'>'.$c->fullname.'</option>';
	}
	print '</select>';
	print '<input type="submit" value="Go">';
	print '</form>';
	echo $OUTPUT->footer();
	exit;
	
}

$context = context_course::instance($course->id);


$url = new moodle_url('/report/activitycompletionreport/csv.php', array('course'=>$id));
$PAGE->set_url($url);
$PAGE->set_pagelayout('report');
require_login();

// Check basic permission
require_capability('report/activitycompletionreport:view',$context);
$group = groups_get_course_group($course,true); // Supposed to verify group
if ($group===0 && $course->groupmode==SEPARATEGROUPS) {
    require_capability('moodle/site:accessallgroups',$context);
}

// Get data on activities and progress of all users, and give error if we've
// nothing to display (no users or no activities)
$reportsurl = $CFG->wwwroot.'/course/report.php?id='.$course->id;
$completion = new completion_info($course);
$activities = $completion->get_activities();

// Generate where clause
$where = array();
$where_params = array();

if ($sifirst !== 'all') {
    $where[] = $DB->sql_like('u.firstname', ':sifirst', false);
    $where_params['sifirst'] = $sifirst.'%';
}

if ($silast !== 'all') {
    $where[] = $DB->sql_like('u.lastname', ':silast', false);
    $where_params['silast'] = $silast.'%';
}

// Get user match count
$total = $completion->get_num_tracked_users(implode(' AND ', $where), $where_params, $group);

    // Navigation and header

$strreports = get_string("reports");
$strcompletion = get_string('activitycompletion', 'completion');
$PAGE->set_title($strcompletion);
$PAGE->set_heading($course->fullname);
$PAGE->requires->jquery();
$PAGE->requires->js('/report/activitycompletionreport/js/jquery-ui.js',true);
$PAGE->requires->js('/report/activitycompletionreport/js/jquery.dataTables.yadcf.js',true);
$PAGE->requires->js('/report/activitycompletionreport/js/jquery.datatables.min.js',true);
$PAGE->requires->js('/report/activitycompletionreport/js/ac.js',true);
$PAGE->requires->css('/report/activitycompletionreport/css/jquery-ui.css');
$PAGE->requires->css('/report/activitycompletionreport/css/jquery.dataTables.min.css');
$PAGE->requires->css('/report/activitycompletionreport/css/jquery.dataTables.yadcf.css');
echo $OUTPUT->header();
echo $OUTPUT->container('<h1>Detailed Course Completion Report</h1>');

//groups_print_course_menu($course,$CFG->wwwroot.'/report/activitycompletionreport/?course='.$course->id);

print '<input type ="hidden" id="courseid" value ="'.$id.'">';
print '<div id="completion-progress-wrapper">';
print '<form name = "courselect" style="float:right;">';
print 'Course: <select name="course">';
foreach($courses as $c){
    if($course->id == $c->id){
        print '<option value ='.$c->id.' selected>'.$c->fullname.'</option>';
    }else{
        print '<option value ='.$c->id.'>'.$c->fullname.'</option>';
    }
    
}
print '</select>';
print '<input type="submit" value="Go">';
print '</form>';
print '<div id="tableDiv">';
print '<div class="processingdiv"><span class="fa fa-spinner fa-spin"></span>&nbsp;&nbsp;&nbsp;Processing</div>';
print '</div>';
print '</div>';
//print $pagingbar;
if($total){
    print '<ul class="progress-actions"><li><a href="csv.php?course='.$course->id.
    '&amp;format=csv">'.get_string('csvdownload','completion').'</a></li>
    <li><a href="csv.php?course='.$course->id.'&amp;format=excelcsv">'.
    get_string('excelcsvdownload','completion').'</a></li></ul>';
}


echo $OUTPUT->footer();

