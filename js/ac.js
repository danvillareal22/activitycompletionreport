$(document).ready(function() {
	var id = $('#courseid').val();
	var url = 'getData.php?course='+id;
	var table = null;
	$.ajax({
                "url": url,
                "success": function(json) {
                    var tableHeaders;
                    $.each(json.columns, function(i, val){
                        tableHeaders += "<th>" + val + "</th>";
                    });
                     
                    $("#tableDiv").empty();
                    $("#tableDiv").append('<table cellspacing="5" cellpadding="5" border="0"><tbody><tr>    <td>Employee No:</td>    <td><span id="empno"></span></td></tr><tr>    <td>Full Name:</td>    <td><span id="fname"></span></td></tr><tr>    <td>Email Address:</td>    <td><span id="email"></span></td></tr><tr>    <td>Area:</td>    <td><span id="area"></span></td></tr><tr>    <td>Zone:</td>    <td><span id="zone"></span></td></tr><tr>    <td>Station:</td>    <td><span id="station"></span></td></tr></tbody></table><table id="completion-progress" class="generaltable flexible boxaligncenter" style="text-align:left"><thead><tr>' + tableHeaders + '</tr></thead></table>');
                    //$("#tableDiv").find("table thead tr").append(tableHeaders);  
					table = $('#completion-progress').DataTable(json);
				   
				    yadcf.init(table,[{
					column_number: 0, 
					filter_type: 'auto_complete', 
					filter_container_id: 'empno',   
					filter_default_label: '', 
					select_type_options: {width: '100%'},
					column_data_type: 'rendered_html',
					},
					{
					column_number: 1, 
					filter_type: 'auto_complete',
					//select_type: 'select',  
					filter_container_id: 'fname',   
					filter_default_label: '', 
					select_type_options: {width: '100%'},
					column_data_type: 'rendered_html',
					},
					{
					column_number: 2, 
					filter_type: 'auto_complete', 
					//select_type: 'select',  
					filter_container_id: 'email',   
					filter_default_label: '', 
					select_type_options: {width: '100%'},
					column_data_type: 'rendered_html',
					},
					{
					column_number: 3, 
					filter_type: 'auto_complete',
					//select_type: 'select',  
					filter_container_id: 'area',   
					filter_default_label: '', 
					select_type_options: {width: '100%'},
					column_data_type: 'rendered_html',
					},
					{
					column_number: 4, 
					filter_type: 'auto_complete', 
					//select_type: 'select',  
					filter_container_id: 'zone',   
					filter_default_label: '', 
					select_type_options: {width: '100%'},
					column_data_type: 'rendered_html',
					},
					{
					column_number: 5, 
					filter_type: 'auto_complete',
					//select_type: 'select',  
					filter_container_id: 'station',   
					filter_default_label: '', 
					select_type_options: {width: '100%'},
					column_data_type: 'rendered_html',
					}]);
                },
                "dataType": "json"
            });
} );